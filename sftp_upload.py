import pysftp
import os

# Change accordingly
hostname = "localhost"
username = "myusername"
password = "mypassword"
port = 22
remote_dir = "/remote_upload_dir/test"
local_dir = "/home/yjj/Desktop/output1"


def get_remote_dir_files(sftp):
    dir_names, file_names, un_name = [],[],[]

    def store_files_name(fname): file_names.append(fname)
    def store_dir_name(dirname): dir_names.append(dirname)
    def store_other_file_types(name): un_name.append(name)

    sftp.walktree(remote_dir, store_files_name, store_dir_name, store_other_file_types, recurse=True)
    return dir_names, file_names


def delete_remote_dir(sftp, remote_dir):
    # Fail-safe method
    if not sftp.exists(remote_dir): return

    # Step 1: Get all directory and files
    dir_names, file_names = get_remote_dir_files(sftp)

    # Step 2: Delete all files
    for afile in file_names:
        sftp.remove(afile)

    # Step 3: Delete all directories (should be empty now)
    dir_names = sorted(dir_names, key=len, reverse=True)  # to ensure delete from the "most sub-dir" first
    for adir in dir_names:
        sftp.rmdir(adir)


# https://stackoverflow.com/questions/14819681/upload-files-using-sftp-in-python-but-create-directories-if-path-doesnt-exist
def create_remote_dir(sftp, remote, is_dir=False):
    dirs_ = []
    if is_dir:
        dir_ = remote
    else:
        dir_, basename = os.path.split(remote)
    while len(dir_) > 1:
        dirs_.append(dir_)
        dir_, _  = os.path.split(dir_)

    if len(dir_) == 1 and not dir_.startswith("/"):
        dirs_.append(dir_)  # For a remote path like y/x.txt

    while len(dirs_):
        dir_ = dirs_.pop()
        try:
            sftp.stat(dir_)
        except:
            sftp.mkdir(dir_)


if __name__ == "__main__":
    print("\n=== Running SFTP Upload ===")

    # Step 1: Connect to SFTP Server
    cnopts = pysftp.CnOpts()  # do not remove
    cnopts.hostkeys = None    # do not remove
    with pysftp.Connection(hostname, username=username, password=password, port=port, cnopts=cnopts) as sftp:
        print("\nConnected to: {}".format(hostname))

        # Step 2: Delete remote dir if exists
        if sftp.exists(remote_dir):
            delete_remote_dir(sftp, remote_dir)

        # Step 3: Create remote dir
        create_remote_dir(sftp, remote_dir, is_dir=True)

        # Step 4: Recursively upload all dir and files
        sftp.put_r(local_dir, remote_dir)

        # Step 5: Just to check the dir and files uploaded to the remote server
        dir_names, file_names = get_remote_dir_files(sftp)
        print("Uploaded Content:")
        print("\tDir: {}".format(dir_names))
        print("\tFiles: {}".format(file_names))

    print("\nBye!")
