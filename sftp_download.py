import pysftp
import os
import shutil


# Change accordingly
hostname = "localhost"
username = "myusername"
password = "mypassword"
port = 22
remote_dir = "/remote_download_dir"
local_dir = "/home/yjj/Desktop/output1"


def prepare_local_dir(local_dir):
    # Step 1: Check if local directory exist
    print("Prepare local directory: {}".format(local_dir))
    dir_exist = os.path.isdir(local_dir)

    # Step 2: If directory exist -> Delete entire directory.
    if dir_exist: shutil.rmtree(local_dir)

    # Step 3: Create directory
    os.mkdir(local_dir)


def print_remote_dir_files(sftp):
    dir_names, file_names, un_name = [],[],[]

    def store_files_name(fname): file_names.append(fname)
    def store_dir_name(dirname): dir_names.append(dirname)
    def store_other_file_types(name): un_name.append(name)

    sftp.walktree(remote_dir, store_files_name, store_dir_name, store_other_file_types, recurse=True)
    print("Content:")
    print("\tDir: {}".format(dir_names))
    print("\tFile: {}".format(file_names))
    print()


def download_dir_files(sftp, remote_dir, local_dir):
    sftp.get_r(remote_dir, local_dir)



if __name__ == "__main__":
    print("\n=== Running SFTP Download ===")

    # Step 1: Prepare local directory
    prepare_local_dir(local_dir)

    # Step 2: Connect to SFTP Server
    print("Connect to SFTP Server")

    cnopts = pysftp.CnOpts()  # do not remove
    cnopts.hostkeys = None    # do not remove
    with pysftp.Connection(hostname, username=username, password=password, port=port, cnopts=cnopts) as sftp:
        print("\nConnected to: {}".format(hostname))

        with sftp.cd(remote_dir):    # Go to specific directory
            print("Inside remote folder: {}".format(remote_dir))
            print_remote_dir_files(sftp)
            download_dir_files(sftp, remote_dir, local_dir)

    print("Bye!")
