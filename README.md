# SFTP CLIENT

### Introduction
This is a python SFTP Client to either:
    <ol>
        <li>Download folders / flles from a SFTP server</li>
        <li>Upload folders / files to a SFTP server</li>
    </ol>
    

### Setting up SFTP server
This is to allow quick test of your client code using Docker SFTP Server.

Ref: `https://hub.docker.com/r/atmoz/sftp/` 

```
docker run \
    -v /home/yjj/Desktop/remote_download_dir:/home/myusername/remote_download_dir \
    -v /home/yjj/Desktop/remote_upload_dir:/home/myusername/remote_upload_dir \
    -p 22:22 -d atmoz/sftp \
    myusername:mypassword:1000
```

After creating the above docker SFTP server container:
```
sftp username: `myusername`
sftp password: `mypassword`
sftp port: `22`
sftp remote folders creared: /remote_download_dir and /remote_upload_dir

/remote_download_dir => mapped to local /home/yjj/Desktop/remote_download_dir
/remote_upload_dir   => mapped to local /home/yjj/Desktop/remote_upload_dir
```

### Test SFTP Server using Command Line
```
sftp myusername@locahost
[password: mypassword]

Commonly used FTP commands:
    help | ls | mkdir / rmdir | put / rm
```

